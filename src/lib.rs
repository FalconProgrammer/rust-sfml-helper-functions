extern crate sfml;

pub fn get_local_size(text: &sfml::graphics::Text) -> sfml::system::Vector2f {
	let text_string = text.string();

	let mut max_line_width: f32 = 0.0;
	let mut line_width = 0.0;
	let mut lines = 0;

	for c in text_string.to_rust_string().chars() {
		match c {
			'\n' => {
				lines += 1;
				max_line_width = max_line_width.max(line_width);
				line_width = 0.0;
			}
			_ => {
				line_width += text
					.font()
					.unwrap()
					.glyph(c as u32, text.character_size(), false, text.outline_thickness())
					.advance()
			}
		}
	}

	if line_width > max_line_width {
		max_line_width = line_width;
		lines += 1;
	}

	let line_height = text.font().unwrap().line_spacing(text.character_size());

	sfml::system::Vector2f {
		x: max_line_width,
		y: lines as f32 * line_height as f32,
	}
}

pub fn create_line(
	point_a: sfml::system::Vector2f,
	point_b: sfml::system::Vector2f,
	thickness: f32,
	colour_a: sfml::graphics::Color,
	colour_b: Option<sfml::graphics::Color>,
) -> Option<sfml::graphics::VertexArray> {
	let colour_b = if let Some(c) = colour_b { c } else { colour_a };

	if thickness > 1.0 {
		let mut array = sfml::graphics::VertexArray::new(sfml::graphics::PrimitiveType::QUADS, 0);

		let delta_x = point_b.x - point_a.x;
		let delta_y = point_b.y - point_a.y;

		let dlength = ((delta_x * delta_x) + (delta_y * delta_y)).sqrt();
		let radius: i32 = ((thickness - 0.99) / 2.0) as i32;

		let dx = delta_x / dlength;
		let dy = delta_y / dlength;
		let nx = dy;
		let ny = -dx;

		array.append(&sfml::graphics::Vertex::new(
			sfml::system::Vector2f::new(
				point_a.x + 0.5 + ((radius as f32 + 0.5) * nx) - (dx * 0.5),
				point_a.y + 0.5 + ((radius as f32 + 0.5) * ny) - (dy * 0.5),
			),
			colour_a,
			sfml::system::Vector2f::new(0.0, 0.0),
		));

		array.append(&sfml::graphics::Vertex::new(
			sfml::system::Vector2f::new(
				point_a.x + 0.5 - ((radius as f32 + 0.5) * nx) - (dx * 0.5),
				point_a.y + 0.5 - ((radius as f32 + 0.5) * ny) - (dy * 0.5),
			),
			colour_a,
			sfml::system::Vector2f::new(0.0, 0.0),
		));

		array.append(&sfml::graphics::Vertex::new(
			sfml::system::Vector2f::new(
				point_b.x + 0.5 - ((radius as f32 + 0.5) * nx) - (dx * 0.5),
				point_b.y + 0.5 - ((radius as f32 + 0.5) * ny) - (dy * 0.5),
			),
			colour_b,
			sfml::system::Vector2f::new(0.0, 0.0),
		));

		array.append(&sfml::graphics::Vertex::new(
			sfml::system::Vector2f::new(
				point_b.x + 0.5 + ((radius as f32 + 0.5) * nx) - (dx * 0.5),
				point_b.y + 0.5 + ((radius as f32 + 0.5) * ny) - (dy * 0.5),
			),
			colour_b,
			sfml::system::Vector2f::new(0.0, 0.0),
		));

		return Some(array);
	} else if thickness == 1.0 {
		let mut array = sfml::graphics::VertexArray::new(sfml::graphics::PrimitiveType::LINES, 0);

		array.append(&sfml::graphics::Vertex::new(point_a, colour_a, sfml::system::Vector2f::new(0.0, 0.0)));
		array.append(&sfml::graphics::Vertex::new(point_b, colour_b, sfml::system::Vector2f::new(0.0, 0.0)));

		return Some(array);
	} else {
		return None;
	}
}


use image::GenericImageView;
use sfml::graphics::Transformable;

pub fn draw_tiled_texture<Target: sfml::graphics::RenderTarget>(texture: &mut Target, images: &[image::DynamicImage]) {
	let window_size = texture.size();

	// NOTE (JB) Calculate the Number of Rows/Columns
	let tile_col_count = ((images.len() as f32).sqrt() + 1.0) as u32;
	let tile_row_count = tile_col_count - (((tile_col_count * tile_col_count) - images.len() as u32) as f32 / tile_col_count as f32) as u32;

	// NOTE (JB) Calculate the starting position of tiles on the last row
	let last_row_gap = if images.len() as f32 % tile_col_count as f32 == 0.0 { 0 } else {tile_col_count - (images.len() as f32 % tile_col_count as f32) as u32};

	// NOTE (JB) Determine Tile Width/Height
	let tile_width = window_size.x / tile_col_count;
	let tile_height = window_size.y / tile_row_count;

	// NOTE (JB) Clear background
	texture.clear(sfml::graphics::Color::BLACK );

	// NOTE (JB) Convert all the images to textures

	let textures = images.iter().map(|image| {
		let rgbimage = image::DynamicImage::ImageRgba8(image.to_rgba8());

		assert_eq!((rgbimage.width() * rgbimage.height() * 4) as usize, rgbimage.to_bytes().len());

		let image = sfml::graphics::Image::create_from_pixels(rgbimage.width(), rgbimage.height(), &rgbimage.to_bytes()).unwrap();

		sfml::graphics::Texture::from_image(&image).unwrap()
	}).collect::<Vec<_>>();

	// NOTE (JB) Draw all the tiles
	let mut tile_x_pos = 0;
	let mut tile_y_pos = 0;

	for (tile_text, orig_image) in textures.iter().zip(images) {
		// NOTE (JB) Create SFML Sprite
		let mut sprite = sfml::graphics::Sprite::with_texture(tile_text);

		// NOTE (JB) Determine / set scale
		let scale_x = tile_width as f32 / orig_image.width() as f32;
		let scale_y = tile_height as f32 / orig_image.height() as f32;
		sprite.set_scale((scale_x, scale_y));

		// NOTE (JB) Determine / set position
		let mut top_left_x = tile_x_pos * tile_width;
		let top_left_y = tile_y_pos * tile_height;

		if tile_y_pos == tile_row_count - 1 && tile_row_count > 1 {
			top_left_x += last_row_gap * tile_width / 2;
		}

		sprite.set_position(sfml::system::Vector2f::new(top_left_x as f32, top_left_y as f32));

		// NOTE (JB) Draw
		texture.draw(&sprite);

		// NOTE (JB) Determine next tile position
		tile_x_pos = if tile_x_pos + 1 >= tile_col_count {
			tile_y_pos += 1;
			0
		}
		else {
			tile_x_pos + 1
		};
	}
}